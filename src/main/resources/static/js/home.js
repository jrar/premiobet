$(function() {

	loadSlider();

});

function loadSlider() {
	$('.bxslider').bxSlider({
		mode : 'fade',
		auto : true,
		autoStart : true,
		autoHover : true,
		pause : 5000
	});
}
