$(function() {

	addClickListeners();
	loadClock();

});

function addClickListeners() {
	$("#bet-type-simple").click(function() {
		$("#bet-type-simple").addClass("betmode-active");
		$("#bet-type-combined").removeClass("betmode-active");
	});
	$("#bet-type-combined").click(function() {
		$("#bet-type-combined").addClass("betmode-active");
		$("#bet-type-simple").removeClass("betmode-active");
	});
	$(".selection").click(
			function() {
				var selectionId = $(this).data("selection-id");
				if (!alreadyInCoupon(selectionId)) {
					getSelectionFragment(selectionId);
					$(".selection[data-selection-id='" + selectionId + "']")
							.addClass("betmode-active");
				}
			});
	$("#refresh-user-balance").click(function() {
		$(this).addClass("fa-spin");
		refreshBalance();
		setTimeout(function() {
			$("#refresh-user-balance").removeClass("fa-spin");
		}, 1000);
	});
	$(".pending-bet-header").click(
			function() {
				var betId = $(this).parents(".pending-bet").data(
						"pendingbet-id");
				var selector = ".pending-bet[data-pendingbet-id=" + betId
						+ "] > .pending-bet-body";
				var status = $(selector).css("display");
				if (status === "none") {
					$(selector).css("display", "block");
				} else {
					$(selector).css("display", "none");
				}
			});
}

function getSelectionFragment(selectionId) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$.ajax({
		type : "POST",
		url : "/bet/get-selection",
		data : selectionId,
		processData : false,
		contentType : "text/html; charset=UTF-8",
		success : function(data) {
			addToCoupon(data);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			window.location.replace("/");
		}
	});
}

function addToCoupon(data) {
	selectionDiv = $.parseHTML(data);
	var selectionId = $(selectionDiv).data("selection-id");
	$("#selected-bets").append(selectionDiv);
	$(".coupon-selection[data-selection-id=" + selectionId + "] .remove-selection").click(
			function() {
				$(".selection[data-selection-id='" + $(this).parents(".coupon-selection").data("selection-id") + "']")
				.removeClass("betmode-active");
				$(this).parents(".coupon-selection").remove();
			});
	$(".coupon-selection[data-selection-id=" + selectionId + "] input[name='placebet']").click(function() {
		placeBet(selectionId);
	});
}

function alreadyInCoupon(selectionId) {
	var exists = false;
	var children = $("#selected-bets").children();
	for (var i = 0; i < children.length; i++) {
		if ($(children[i]).data("selection-id") == selectionId) {
			exists = true;
		}
	}
	return exists;
}

function placeBet(selectionId) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var odd = $(".coupon-selection[data-selection-id='" + selectionId + "']")
			.data("odd");
	var amount = $(
			".coupon-selection[data-selection-id='" + selectionId
					+ "'] input[name='amount']").val();
	var bet = {
		"selectionId" : selectionId,
		"odd" : odd,
		"amount" : amount
	};
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$.ajax({
		type : "POST",
		url : "/bet/place-bet",
		data : JSON.stringify(bet),
		processData : false,
		contentType : "application/json",
		success : function(data) {
			var betResult = $.parseHTML(data);
			$(".coupon-selection[data-selection-id='" + selectionId + "']")
					.replaceWith(betResult);
			$(
					".coupon-selection[data-selection-id=" + selectionId
							+ "] .remove-selection").click(
					function() {
						$(
								".selection[data-selection-id='"
										+ $(this).parents(".coupon-selection")
												.data("selection-id") + "']")
								.removeClass("betmode-active");
						$(this).parents(".coupon-selection").remove();
					});
			$(
					".coupon-selection[data-selection-id=" + selectionId
							+ "] input[name='placebet']").click(function() {
				placeBet(selectionId);
			});
			refreshBalance();
			refreshPendingBets();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			window.location.replace("/");
		}
	});
}

function refreshBalance() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$.ajax({
		type : "POST",
		url : "/user/refresh-balance",
		processData : false,
		contentType : "text/html; charset=UTF-8",
		success : function(data) {
			$("#user-balance").replaceWith(data);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			window.location.replace("/");
		}
	});
}

function loadClock() {
	function refreshClock() {
		var hours = parseInt($('#clock-hours').html());
		var minutes = parseInt($('#clock-minutes').html());
		var seconds = parseInt($('#clock-seconds').html());

		if ((seconds + 1) < 60) {
			seconds += 1;
			if (seconds >= 10) {
				$('#clock-seconds').html(seconds);
			} else {
				$('#clock-seconds').html("0" + seconds);
			}
		} else {
			$('#clock-seconds').html("00");
			minutes += 1;
			if (minutes < 60) {
				$('#clock-minutes').html(minutes);
				if (minutes >= 10) {
					$('#clock-minutes').html(minutes);
				} else {
					$('#clock-minutes').html("0" + minutes);
				}
			} else {
				$('#clock-minutes').html("00");
				hours += 1;
				if (hours < 24) {
					if (hours >= 10) {
						$('#clock-hours').html(hours);
					} else {
						$('#clock-hours').html("0" + hours);
					}
				} else {
					$('#clock-hours').html("00");
					refreshDate();
				}
			}
		}
	}
	setInterval(refreshClock, 1000);
}

function refreshDate() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$.ajax({
		type : "POST",
		url : "/refresh-date",
		processData : false,
		contentType : "text/html; charset=UTF-8",
		success : function(data) {
			$("#server-date").replaceWith(data);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			window.location.replace("/");
		}
	});
}

function refreshPendingBets() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	$.ajax({
		type : "POST",
		url : "/bet/refresh-pending-bets",
		processData : false,
		contentType : "text/html; charset=UTF-8",
		success : function(data) {
			$("#pending-bets").replaceWith(data);
			$(".pending-bet-header").click(
					function() {
						var betId = $(this).parents(".pending-bet").data(
								"pendingbet-id");
						var selector = ".pending-bet[data-pendingbet-id="
								+ betId + "] > .pending-bet-body";
						var status = $(selector).css("display");
						if (status === "none") {
							$(selector).css("display", "block");
						} else {
							$(selector).css("display", "none");
						}
					});
		},
		error : function(jqXHR, textStatus, errorThrown) {
			window.location.replace("/");
		}
	});
}