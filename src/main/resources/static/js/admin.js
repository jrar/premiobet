$(function() {

	addListeners();

});

function addListeners() {
	$("#competition").change(function() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
		$.ajax({
			type : "POST",
			url : "/admin/get-competition-events",
			data : $("#competition").val(),
			processData : false,
			contentType : "text/html; charset=UTF-8",
			success : function(data) {
				var events = $.parseHTML(data);
				$(".replace-event").replaceWith(events);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log("error");
				alert(errorThrown);
			}
		});
		

	});

	$(".close-event-form").submit(
			function(event) {
				
				event.preventDefault();
				var currentHtml = this;
				var marketId = $(this).data("market-id");
				var selectionId = $(
						"form[data-market-id=" + marketId + "] select").val();
				var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
				var market = {
					"marketId" : marketId,
					"selectionId" : selectionId,
				};
				$(document).ajaxSend(function(e, xhr, options) {
					xhr.setRequestHeader(header, token);
				});
				$.ajax({
					type : "POST",
					url : "/admin/events/close/do",
					data : JSON.stringify(market),
					processData : false,
					contentType : "application/json",
					success : function(data) {
						$(currentHtml).replaceWith(data);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log("error");
						alert(jqXHR.status);
					}
				});
			});
}
