package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Competition;

@Repository("competitionRepository")
public interface CompetitionRepository extends JpaRepository<Competition, Serializable> {
	
	public abstract Competition findById(int id);
	
	@Query(value = "SELECT * FROM competitions WHERE sport_id=:sportId AND parent_id=0 ORDER BY competition_id", nativeQuery = true)
	public List<Competition> findParentCompetitionsBySportId(@Param("sportId") int sportId);
	
	@Query(value = "SELECT * FROM competitions WHERE parent_id=:parentId ORDER BY competition_id", nativeQuery = true)
	public List<Competition> findChildrenCompetitions(@Param("parentId") int parentId);
	
	@Query(value = "SELECT COUNT(c.competition_id) FROM competitions c, events e WHERE c.competition_id=e.competition_id AND c.competition_id=:competitionId"
			+ " AND e.start_date>NOW()", nativeQuery = true)
	public int getEventCountForSingleCompetition(@Param("competitionId") int competitionId);	
	
	@Query(value = "SELECT * FROM competitions ORDER BY name", nativeQuery = true)
	public List<Competition> findAll();	
	
	public abstract void delete(Competition competition);
	
}
