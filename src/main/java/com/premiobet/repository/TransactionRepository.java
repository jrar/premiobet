package com.premiobet.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Transaction;

@Repository("transactionRepository")
public interface TransactionRepository extends JpaRepository<Transaction, Serializable> {
	
	public abstract Transaction findById(int id);
	
	@Query(value = "SELECT COUNT(*) FROM transactions WHERE type='prize'", nativeQuery = true)
	public int findRedeemedPrizesCount();

}
