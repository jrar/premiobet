package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Selection;

@Repository("selectionRepository")
public interface SelectionRepository extends JpaRepository<Selection, Serializable> {

	public abstract Selection findById(int id);

	@Query(value = "SELECT s.* FROM events e, markets m, selections s WHERE m.category='Principal'"
			+ " AND s.market_id=m.market_id AND e.event_id=:eventId AND e.event_id=m.event_id"
			+ " ORDER BY s.selection_id ASC", nativeQuery = true)
	public List<Selection> findMainMarket(@Param("eventId") int eventId);

	@Query(value = "SELECT s.* FROM markets m, selections s"
			+ " WHERE s.market_id=m.market_id AND m.market_id=:marketId"
			+ " ORDER BY s.selection_id ASC", nativeQuery = true)
	public List<Selection> findMarketSelections(@Param("marketId") int marketId);

	@Query(value = "SELECT s.* FROM markets m, selections s"
			+ " WHERE s.market_id=m.market_id AND m.market_id=:marketId AND m.result=0"
			+ " ORDER BY s.selection_id ASC", nativeQuery = true)
	public List<Selection> findPendingMarketSelections(@Param("marketId") int marketId);

	@Query(value = "SELECT s.* FROM markets m, selections s"
			+ " WHERE s.market_id=m.market_id AND m.market_id=:marketId AND s.result_id=:result", nativeQuery = true)
	public Selection getMarketWinnerSelection(@Param("marketId") int marketId, @Param("result") int result);

}
