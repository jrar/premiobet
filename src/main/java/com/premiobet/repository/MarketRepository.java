package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.premiobet.entity.Market;

@Repository("marketRepository")
public interface MarketRepository extends JpaRepository<Market, Serializable> {

	public abstract Market findById(int id);

	@Query(value = "SELECT COUNT(*) FROM markets m, events e WHERE m.event_id=e.event_id AND e.event_id=:eventId", nativeQuery = true)
	public int getMarketCountForSingleEvent(@Param("eventId") int eventId);

	@Query(value = "SELECT m.* FROM markets m, events e WHERE m.event_id=e.event_id "
			+ "AND e.event_id=:eventId ORDER BY m.market_id ASC", nativeQuery = true)
	public List<Market> findEventMarkets(@Param("eventId") int eventId);
	
	@Query(value = "SELECT m.* FROM markets m, events e WHERE m.event_id=e.event_id "
			+ "AND e.event_id=:eventId AND m.category='Principal'", nativeQuery = true)
	public Market findEventMainMarket(@Param("eventId") int eventId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE markets SET result=:result WHERE market_id=:marketId", nativeQuery = true)
	public void updateMarketResult(@Param("marketId") int marketId, @Param("result") int result);

}
