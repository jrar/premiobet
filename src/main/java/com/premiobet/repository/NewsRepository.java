package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.News;

@Repository("newsRepository")
public interface NewsRepository extends JpaRepository<News, Serializable> {

	@Query(value = "SELECT * FROM news ORDER BY news_id DESC LIMIT 4", nativeQuery = true)
	public List<News> findLatestNews();

}
