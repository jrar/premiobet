package com.premiobet.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Prize;

@Repository("prizeRepository")
public interface PrizeRepository extends JpaRepository<Prize, Serializable> {
	
	public abstract Prize findById(int id);
	
	@Query(value = "SELECT COUNT(*) FROM prizes WHERE available=true", nativeQuery = true)
	public int findAvailablePrizesCount();

}
