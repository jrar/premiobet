package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.premiobet.entity.Bet;

@Repository("betRepository")
public interface BetRepository extends JpaRepository<Bet, Serializable> {

	public abstract Bet findById(int id);

	@Query(value = "SELECT b.* FROM bets b, markets m WHERE b.user_id=:userId "
			+ "AND b.market_id=m.market_id AND m.result=0 ORDER BY b.date DESC LIMIT :limit", nativeQuery = true)
	public List<Bet> findUserPendingBets(@Param("userId") int userId, @Param("limit") int limit);

	@Query(value = "SELECT COUNT(*) FROM bets WHERE result=0", nativeQuery = true)
	public int findPendingBetsCount();

	@Query(value = "SELECT COUNT(*) FROM bets", nativeQuery = true)
	public int findTotalBetsCount();

	@Modifying
	@Transactional
	@Query(value = "UPDATE bets SET result=1, income=(amount*odds) WHERE selection_id=:selectionId", nativeQuery = true)
	public void updateWinnerBetBySelectionId(@Param("selectionId") int selectionId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE bets SET result=2, income=0 WHERE selection_id=:selectionId", nativeQuery = true)
	public void updateLoserBetBySelectionId(@Param("selectionId") int selectionId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE bets SET result=-1, income=amount WHERE selection_id=:selectionId", nativeQuery = true)
	public void updateCanceledBetBySelectionId(@Param("selectionId") int selectionId);
	
	@Query(value = "SELECT SUM(income) FROM bets WHERE selection_id=:selectionId AND user_id=:userId", nativeQuery = true)
	public double findUserIncome(@Param("selectionId") int selectionId, @Param("userId") int userId);
	
	@Query(value = "SELECT DISTINCT user_id FROM bets WHERE selection_id=:selectionId", nativeQuery = true)
	public int[] findWinnerUsers(@Param("selectionId") int selectionId);

}
