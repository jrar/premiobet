package com.premiobet.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.premiobet.entity.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Serializable> {
	
	public abstract User findByUsername(String username);
	public abstract User findByEmail(String email);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE users SET balance=(balance-:betAmount) WHERE user_id=:userId", nativeQuery = true)
	public void updateBalance(@Param("betAmount") double betAmount, @Param("userId") int userId);
	
	@Query(value = "SELECT COUNT(*) FROM users WHERE enabled=true", nativeQuery = true)
	public int findEnabledUsersCount();
	
	@Query(value = "SELECT COUNT(*) FROM users WHERE enabled=true AND verified=true", nativeQuery = true)
	public int findEnabledVerifiedUsersCount();
	
	@Query(value = "SELECT balance FROM users WHERE user_id=:userId", nativeQuery = true)
	public double findUserBalance(@Param("userId") int userId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE users SET balance=(balance+:amount) WHERE user_id=:userId", nativeQuery = true)
	public void addBalance(@Param("amount") double amount, @Param("userId") int userId);

}
