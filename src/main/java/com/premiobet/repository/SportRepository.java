package com.premiobet.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Sport;

@Repository("sportRepository")
public interface SportRepository extends JpaRepository<Sport, Serializable> {
	
	public abstract Sport findById(int id);

	public abstract void delete(Sport sport);

}
