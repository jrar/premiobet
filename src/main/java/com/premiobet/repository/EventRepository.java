package com.premiobet.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.premiobet.entity.Event;

@Repository("eventRepository")
public interface EventRepository extends JpaRepository<Event, Serializable> {

	public abstract Event findById(int id);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE c.sport_id=:sportId "
			+ "AND e.competition_id=c.competition_id AND e.start_date>NOW() "
			+ "ORDER BY e.bets DESC LIMIT :limit", nativeQuery = true)
	public List<Event> findPopularEventsBySportId(@Param("sportId") int sportId, @Param("limit") int limit);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE "
			+ "e.competition_id=c.competition_id AND e.start_date>NOW() "
			+ "ORDER BY e.bets DESC LIMIT :limit", nativeQuery = true)
	public List<Event> findPopularEventsFromAll(@Param("limit") int limit);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE c.sport_id=:sportId "
			+ "AND e.competition_id=c.competition_id AND e.start_date>NOW() "
			+ "ORDER BY e.start_date ASC LIMIT :limit", nativeQuery = true)
	public List<Event> findUpcomingEventsBySportId(@Param("sportId") int sportId, @Param("limit") int limit);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE "
			+ "e.competition_id=c.competition_id AND e.start_date>NOW() "
			+ "ORDER BY e.start_date ASC LIMIT :limit", nativeQuery = true)
	public List<Event> findUpcomingEventsFromAll(@Param("limit") int limit);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE c.competition_id=:competitionId "
			+ "AND e.competition_id=c.competition_id AND e.start_date>NOW() "
			+ "ORDER BY e.start_date ASC", nativeQuery = true)
	public List<Event> findCompetitionEvents(@Param("competitionId") int competitionId);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE c.competition_id=:competitionId "
			+ " AND e.competition_id=c.competition_id ORDER BY e.home ASC", nativeQuery = true)
	public List<Event> findAllCompetitionEvents(@Param("competitionId") int competitionId);

	// Return events that already started but their main market result is
	// undefined yet
	@Query(value = "SELECT e.* FROM events e, markets m WHERE e.start_date<NOW()"
			+ " AND m.event_id=e.event_id AND m.category='Principal' AND m.result=0 ORDER BY e.start_date ASC", nativeQuery = true)
	public List<Event> findPendingEvents();

	// Return events that already started but their main market result is
	// undefined yet BY COMPETITION
	@Query(value = "SELECT e.* FROM events e, markets m WHERE e.start_date<NOW() AND e.competition_id=:competitionId"
			+ " AND m.event_id=e.event_id AND m.category='Principal' AND m.result=0 ORDER BY e.start_date ASC", nativeQuery = true)
	public List<Event> findPendingEventsByCompetition(@Param("competitionId") int competitionId);

	// Count events that already started but their main market result is
	// undefined yet BY COMPETITION
	@Query(value = "SELECT COUNT(e.event_id) FROM events e, markets m WHERE e.start_date<NOW() AND e.competition_id=:competitionId"
			+ " AND m.event_id=e.event_id AND m.category='Principal' AND m.result=0 ORDER BY e.start_date ASC", nativeQuery = true)
	public Integer countPendingEventsByCompetition(@Param("competitionId") int competitionId);

	@Query(value = "SELECT e.* FROM events e, competitions c WHERE c.competition_id=:competitionId AND e.start_date>NOW()"
			+ " AND e.competition_id=c.competition_id AND e.round=:round ORDER BY e.start_date ASC", nativeQuery = true)
	public List<Event> findAllCompetitionEventsByRound(@Param("competitionId") int competitionId,
			@Param("round") int round);

	public abstract void delete(Event event);

}
