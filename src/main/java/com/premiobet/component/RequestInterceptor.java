package com.premiobet.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.premiobet.service.UserService;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

  @Autowired
  private UserService userService;

  @Autowired
  private HttpSession session;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    if (session.getAttribute("user") == null) {
      if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null
          && !SecurityContextHolder.getContext().getAuthentication().getPrincipal()
              .equals("anonymousUser")) {
        session.setAttribute("user", userService.getUserEntity());
      }
    } else {
      if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null
          || SecurityContextHolder.getContext().getAuthentication().getPrincipal()
              .equals("anonymousUser")) {
        session.invalidate();
      }
    }
    return super.preHandle(request, response, handler);
  }

}
