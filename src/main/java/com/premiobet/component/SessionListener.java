package com.premiobet.component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.springframework.stereotype.Component;

@Component
public class SessionListener implements HttpSessionListener {

  @Override
  public void sessionCreated(HttpSessionEvent event) {
    event.getSession().setMaxInactiveInterval(15 * 60);
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent event) {}
}
