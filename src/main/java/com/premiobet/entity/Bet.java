package com.premiobet.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "bets")
@Data
public class Bet {

  @Id
  @GeneratedValue
  @Column(name = "bet_id")
  private int id;

  @Column(name = "user_id", nullable = false, unique = false)
  private int userId;

  @Column(name = "event_id", nullable = false, unique = false)
  private int eventId;

  @Column(name = "market_id", nullable = false, unique = false)
  private int marketId;

  @Column(name = "selection_id", nullable = false, unique = false)
  private int selectionId;

  @Column(name = "amount", nullable = false, unique = false)
  private double amount;

  @Column(name = "odds", nullable = false, unique = false)
  private double odds;

  @Column(name = "date", nullable = false, unique = false)
  private Date date;

  @Column(name = "result", nullable = true, unique = false)
  private int result;

  @Column(name = "income", nullable = true, unique = false)
  private double income;

  public Bet() {}

  public Bet(int userId, int eventId, int marketId, int selectionId, double amount, double odds,
      Date date, int result, double income) {
    this.userId = userId;
    this.eventId = eventId;
    this.marketId = marketId;
    this.selectionId = selectionId;
    this.amount = amount;
    this.odds = odds;
    this.date = date;
    this.result = result;
    this.income = income;
  }

  public Bet(int id, int userId, int eventId, int marketId, int selectionId, double amount,
      double odds, Date date, int result, double income) {
    this.id = id;
    this.userId = userId;
    this.eventId = eventId;
    this.marketId = marketId;
    this.selectionId = selectionId;
    this.amount = amount;
    this.odds = odds;
    this.date = date;
    this.result = result;
    this.income = income;
  }

}
