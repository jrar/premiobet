package com.premiobet.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "events")
@Data
public class Event {

  @Id
  @GeneratedValue
  @Column(name = "event_id")
  private int id;

  @Column(name = "competition_id", nullable = false, unique = false)
  private int competitionId;

  @Column(name = "home", nullable = false, unique = false, length = 70)
  private String home;

  @Column(name = "away", nullable = false, unique = false, length = 70)
  private String away;

  @Column(name = "start_date", nullable = false)
  private Date startDate;

  @Column(name = "bets", nullable = true)
  private int bets;

  @Column(name = "round", nullable = false, unique = false)
  private int round;

  public Event() {}

  public Event(int competitionId, String home, String away, Date startDate, int round) {
    this.competitionId = competitionId;
    this.home = home;
    this.away = away;
    this.startDate = startDate;
    this.round = round;
  }

  public Event(int id, int competitionId, String home, String away, Date startDate, int round) {
    this.id = id;
    this.competitionId = competitionId;
    this.home = home;
    this.away = away;
    this.startDate = startDate;
    this.round = round;
  }

}
