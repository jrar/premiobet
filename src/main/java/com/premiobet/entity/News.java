package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "news")
@Data
public class News {

  @Id
  @GeneratedValue
  @Column(name = "news_id")
  private int id;

  @Column(name = "img", nullable = true, unique = false, length = 120)
  private String img;

  @Column(name = "url", nullable = true, unique = false, length = 200)
  private String url;

  @Column(name = "title", nullable = false, unique = false, length = 90)
  private String title;

  @Column(name = "description", nullable = true, unique = false, length = 250)
  private String description;

  public News(int id, String img, String url, String title, String description) {
    this.id = id;
    this.img = img;
    this.url = url;
    this.title = title;
    this.description = description;
  }

  public News(String img, String url, String title, String description) {
    this.img = img;
    this.url = url;
    this.title = title;
    this.description = description;
  }

  public News() {}

}
