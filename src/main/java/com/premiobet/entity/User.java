package com.premiobet.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class User {

  @Id
  @GeneratedValue
  @Column(name = "user_id")
  private int id;

  @Column(name = "username", nullable = false, unique = true, length = 30)
  private String username;

  @Column(name = "password", nullable = false, length = 60)
  private String password;

  @Column(name = "email", nullable = false, unique = true, length = 60)
  private String email;

  @Column(name = "balance", nullable = false, precision = 12, scale = 2)
  private double balance;

  @Column(name = "first_name", nullable = false, length = 30)
  private String name;

  @Column(name = "last_name", nullable = false, length = 30)
  private String lastName;

  @Column(name = "country", nullable = false, length = 30)
  private String country;

  @Column(name = "birth_date", nullable = false)
  private Date birthDate;

  @Column(name = "enabled", nullable = false)
  private boolean enabled;

  @Column(name = "verified", nullable = false)
  private boolean verified;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
  private Set<UserRole> userRole = new HashSet<UserRole>();

  public User() {}

  public User(String username, String password, String email, String name, String lastName,
      String country, Date birthDate) {
    this.username = username;
    this.password = password;
    this.email = email;
    this.name = name;
    this.lastName = lastName;
    this.country = country;
    this.birthDate = birthDate;
  }

  public User(String username, String password, String email, double balance, String name,
      String lastName, String country, Date birthDate, boolean enabled, boolean verified,
      Set<UserRole> userRole) {
    this.username = username;
    this.password = password;
    this.email = email;
    this.balance = balance;
    this.name = name;
    this.lastName = lastName;
    this.country = country;
    this.birthDate = birthDate;
    this.enabled = enabled;
    this.verified = verified;
    this.userRole = userRole;
  }

}
