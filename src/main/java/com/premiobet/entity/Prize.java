package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "prizes")
@Data
public class Prize {

  @Id
  @GeneratedValue
  @Column(name = "prize_id")
  private int id;

  @Column(name = "name", nullable = false, unique = false, length = 50)
  private String name;

  @Column(name = "cost", nullable = false, unique = false)
  private double cost;

  @Column(name = "type", nullable = false, unique = false, length = 30)
  private String type;

  @Column(name = "category", nullable = false, unique = false, length = 50)
  private String category;

  @Column(name = "available", nullable = false, unique = false)
  private boolean available;

  public Prize() {}

  public Prize(String name, double cost, String type, String category, boolean available) {
    this.name = name;
    this.cost = cost;
    this.type = type;
    this.category = category;
    this.available = available;
  }

  public Prize(int id, String name, double cost, String type, String category, boolean available) {
    this.id = id;
    this.name = name;
    this.cost = cost;
    this.type = type;
    this.category = category;
    this.available = available;
  }

}
