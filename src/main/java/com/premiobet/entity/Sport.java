package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "sports")
@Data
public class Sport {

  @Id
  @GeneratedValue
  @Column(name = "sport_id")
  private int id;

  @Column(name = "name", nullable = false, unique = true, length = 30)
  private String name;

  @Column(name = "img", nullable = false, unique = false, length = 90)
  private String img;

  public Sport() {}

  public Sport(String name, String img) {
    this.name = name;
    this.img = img;
  }

  public Sport(int id, String name, String img) {
    this.id = id;
    this.name = name;
    this.img = img;
  }

}
