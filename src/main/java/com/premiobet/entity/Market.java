package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "markets")
@Data
public class Market {

  @Id
  @GeneratedValue
  @Column(name = "market_id")
  private int id;

  @Column(name = "name", nullable = false, unique = false, length = 50)
  private String name;

  @Column(name = "category", nullable = false, unique = false, length = 80)
  private String category;

  @Column(name = "event_id", nullable = false, unique = false)
  private int event;

  @Column(name = "result", nullable = true, unique = false)
  private int result = 0;

  public Market() {}

  public Market(String name, String category, int event, int result) {
    this.name = name;
    this.category = category;
    this.event = event;
    this.result = result;
  }

  public Market(String name, String category, int event) {
    this.name = name;
    this.category = category;
    this.event = event;
  }

  public Market(int id, String name, String category, int event, int result) {
    this.id = id;
    this.name = name;
    this.category = category;
    this.event = event;
    this.result = result;
  }

}
