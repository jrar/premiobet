package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;

@Entity
@Table(name = "user_roles",
    uniqueConstraints = @UniqueConstraint(columnNames = {"role", "user_id"}))
@Data
public class UserRole {

  @Id
  @GeneratedValue
  @Column(name = "user_role_id", unique = true, nullable = false)
  private int userRoleId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @Column(name = "role", nullable = false, length = 45)
  private String role;

  public UserRole(User user, String role) {
    this.user = user;
    this.role = role;
  }

  public UserRole() {}

}
