package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "selections")
@Data
public class Selection {

  @Id
  @GeneratedValue
  @Column(name = "selection_id")
  private int id;

  @Column(name = "name", nullable = false, unique = false, length = 50)
  private String name;

  @Column(name = "market_id", nullable = false, unique = false)
  private int market;

  @Column(name = "odd", nullable = false)
  private double odd;

  @Column(name = "result_id", nullable = false, unique = false)
  private int resultId;

  public Selection() {}

  public Selection(String name, int market, double odd, int resultId) {
    this.name = name;
    this.market = market;
    this.odd = odd;
    this.resultId = resultId;
  }

  public Selection(int id, String name, int market, double odd, int resultId) {
    this.id = id;
    this.name = name;
    this.market = market;
    this.odd = odd;
    this.resultId = resultId;
  }

}
