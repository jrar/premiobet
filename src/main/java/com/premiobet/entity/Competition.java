package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "competitions")
@Data
public class Competition {

  @Id
  @GeneratedValue
  @Column(name = "competition_id")
  private int id;

  @Column(name = "name", nullable = false, unique = false, length = 50)
  private String name;

  @Column(name = "parent_id", nullable = true, unique = false)
  private int parentId = 0;

  @Column(name = "sport_id", nullable = false, unique = false)
  private int sport;

  public Competition() {}

  public Competition(String name, int parentId, int sport) {
    this.name = name;
    this.parentId = parentId;
    this.sport = sport;
  }

  public Competition(int id, String name, int parentId, int sport) {
    this.id = id;
    this.name = name;
    this.parentId = parentId;
    this.sport = sport;
  }

}
