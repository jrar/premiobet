package com.premiobet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "transactions")
@Data
public class Transaction {

  @Id
  @GeneratedValue
  @Column(name = "transaction_id")
  private int id;

  @Column(name = "type", nullable = false, unique = false, length = 50)
  private String type;

  @Column(name = "user_id", nullable = false, unique = false)
  private int user;

  @Column(name = "amount", nullable = false, unique = false)
  private double amount;

  @Column(name = "description", nullable = false, unique = false, length = 150)
  private String description;

  public Transaction() {}

  public Transaction(String type, int user, double amount, String description) {
    this.type = type;
    this.user = user;
    this.amount = amount;
    this.description = description;
  }

  public Transaction(int id, String type, int user, double amount, String description) {
    this.id = id;
    this.type = type;
    this.user = user;
    this.amount = amount;
    this.description = description;
  }

}
