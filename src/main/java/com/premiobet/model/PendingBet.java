package com.premiobet.model;

import java.io.Serializable;
import com.premiobet.entity.Bet;
import lombok.Data;

@Data
public class PendingBet implements Serializable {

  private static final long serialVersionUID = -5619873816755033751L;
  private Bet bet;
  private String selectionName;
  private String eventName;
  private String marketName;

  public PendingBet(Bet bet, String selectionName, String eventName, String marketName) {
    this.bet = bet;
    this.selectionName = selectionName;
    this.eventName = eventName;
    this.marketName = marketName;
  }

  public PendingBet() {}

}
