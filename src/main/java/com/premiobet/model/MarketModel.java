package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class MarketModel implements Serializable {

  private static final long serialVersionUID = 8913092770461999373L;
  private String name;
  private String category;
  private String home;
  private double homeOdds;
  private String draw;
  private double drawOdds;
  private String away;
  private double awayOdds;
  private int eventId;

  public MarketModel() {}

  public MarketModel(String name, String category, String home, double homeOdds, String draw,
      double drawOdds, String away, double awayOdds) {
    this.name = name;
    this.category = category;
    this.home = home;
    this.homeOdds = homeOdds;
    this.draw = draw;
    this.drawOdds = drawOdds;
    this.away = away;
    this.awayOdds = awayOdds;
  }

}
