package com.premiobet.model;

import java.io.Serializable;
import java.util.List;
import com.premiobet.entity.Market;
import com.premiobet.entity.Selection;
import lombok.Data;

@Data
public class CloseEventModel implements Serializable {

  private static final long serialVersionUID = 1145172341432007123L;
  private Market market;
  private List<Selection> marketSelections;

  public CloseEventModel() {}

  public CloseEventModel(Market market, List<Selection> marketSelections) {
    this.market = market;
    this.marketSelections = marketSelections;
  }

}
