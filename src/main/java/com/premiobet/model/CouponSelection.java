package com.premiobet.model;

import java.io.Serializable;
import com.premiobet.entity.Selection;
import lombok.Data;

@Data
public class CouponSelection implements Serializable {

  private static final long serialVersionUID = 596359385076373107L;
  private Selection selection;
  private String marketName;
  private String event;

  public CouponSelection(Selection selection, String marketName, String event) {
    this.selection = selection;
    this.marketName = marketName;
    this.event = event;
  }

  public CouponSelection() {}

}
