package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class AdminStats implements Serializable {

  private static final long serialVersionUID = -6851151604421835828L;
  private int activeUsers;
  private int verifiedUsers;
  private int pendingBets;
  private int totalBets;
  private int availablePrizes;
  private int redeemedPrizes;

  public AdminStats() {}

  public AdminStats(int activeUsers, int verifiedUsers, int pendingBets, int totalBets,
      int availablePrizes, int redeemedPrizes) {
    this.activeUsers = activeUsers;
    this.verifiedUsers = verifiedUsers;
    this.pendingBets = pendingBets;
    this.totalBets = totalBets;
    this.availablePrizes = availablePrizes;
    this.redeemedPrizes = redeemedPrizes;
  }

}
