package com.premiobet.model;

import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;

@Data
public class EventModel implements Serializable {

  private static final long serialVersionUID = 6680989466205270298L;

  private int id;
  private int competitionId;
  private String home;
  private String away;
  private int round;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date date;

  public EventModel() {}

  public EventModel(int id, int competitionId, String home, String away, Date date, int round) {
    this.id = id;
    this.competitionId = competitionId;
    this.home = home;
    this.away = away;
    this.date = date;
    this.round = round;
  }

  public EventModel(int competitionId, String home, String away, Date date, int round) {
    this.competitionId = competitionId;
    this.home = home;
    this.away = away;
    this.date = date;
    this.round = round;
  }

}
