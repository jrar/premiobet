package com.premiobet.model;

import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;

@Data
public class RegistrationModel implements Serializable {

  private static final long serialVersionUID = -7978098313050415651L;
  private String username;
  private String password;
  private String firstName;
  private String lastName;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date birthDate;
  private String email;
  private String country;

  public RegistrationModel() {}

  public RegistrationModel(String username, String password, String firstName, String lastName,
      Date birthDate, String email, String country) {
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.email = email;
    this.country = country;
  }

}
