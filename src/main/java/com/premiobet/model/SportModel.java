package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class SportModel implements Serializable {

  private static final long serialVersionUID = 1483152027414843030L;
  private String sportName;
  private String iconFile;
  private int id;

  public SportModel() {}

  public SportModel(String sportName, String iconFile) {
    this.sportName = sportName;
    this.iconFile = iconFile;
  }

  public SportModel(String sportName, String iconFile, int id) {
    this.sportName = sportName;
    this.iconFile = iconFile;
    this.id = id;
  }

}
