package com.premiobet.model;

import java.io.Serializable;
import java.util.List;
import com.premiobet.entity.Event;
import com.premiobet.entity.Selection;
import lombok.Data;

@Data
public class EventDetail implements Serializable {

  private static final long serialVersionUID = 4964169274033167294L;
  private Event event;
  private List<Selection> selections;
  private int marketCount;

  public EventDetail() {}

  public EventDetail(Event event, List<Selection> selections, int marketCount) {
    this.event = event;
    this.selections = selections;
    this.marketCount = marketCount;
  }

}
