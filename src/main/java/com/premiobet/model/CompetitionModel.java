package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class CompetitionModel implements Serializable {

  private static final long serialVersionUID = -133076943210892472L;
  private int id;
  private String name;
  private int parentId;
  private int sportId;

  public CompetitionModel() {}

  public CompetitionModel(int id, String name, int parentId, int sportId) {
    this.id = id;
    this.name = name;
    this.parentId = parentId;
    this.sportId = sportId;
  }

  public CompetitionModel(String name, int parentId, int sportId) {
    this.name = name;
    this.parentId = parentId;
    this.sportId = sportId;
  }

}
