package com.premiobet.model;

import java.io.Serializable;
import java.util.List;
import com.premiobet.entity.Competition;
import lombok.Data;

@Data
public class SportCompetition implements Serializable {

  private static final long serialVersionUID = -4126245547904687914L;
  private Competition competition;
  private List<Competition> childrenCompetition;
  private int eventCount;

  public SportCompetition() {}

  public SportCompetition(Competition competition, List<Competition> childrenCompetition,
      int eventCount) {
    this.competition = competition;
    this.childrenCompetition = childrenCompetition;
    this.eventCount = eventCount;
  }

}
