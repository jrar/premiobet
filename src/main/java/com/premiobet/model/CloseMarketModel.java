package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class CloseMarketModel implements Serializable {

  private static final long serialVersionUID = -7343618752538568066L;
  private int marketId;
  private int selectionId;

  public CloseMarketModel() {}

  public CloseMarketModel(int marketId, int selectionId) {
    this.marketId = marketId;
    this.selectionId = selectionId;
  }

}
