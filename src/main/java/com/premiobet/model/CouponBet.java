package com.premiobet.model;

import java.io.Serializable;
import lombok.Data;

@Data
public class CouponBet implements Serializable {

  private static final long serialVersionUID = -8339474818456383102L;
  private int selectionId;
  private double odd;
  private double amount;

  public CouponBet() {}

  public CouponBet(int selectionId, double odd, double amount) {
    this.selectionId = selectionId;
    this.odd = odd;
    this.amount = amount;
  }

}
