package com.premiobet.model;

import java.io.Serializable;
import java.util.List;
import com.premiobet.entity.Market;
import com.premiobet.entity.Selection;
import lombok.Data;

@Data
public class EventMarket implements Serializable {

  private static final long serialVersionUID = -5722354330398679595L;
  private Market market;
  private List<Selection> marketSelections;

  public EventMarket() {}

  public EventMarket(Market market, List<Selection> marketSelections) {
    this.market = market;
    this.marketSelections = marketSelections;
  }

}
