package com.premiobet.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserDetailsService userService;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        .sessionAuthenticationErrorUrl("/login?error").maximumSessions(1)
        .maxSessionsPreventsLogin(false).expiredUrl("/login?expired").and().sessionFixation()
        .newSession();

    http.authorizeRequests().antMatchers("/admin", "/admin/**").access("hasRole('ADMIN')")
        .antMatchers("/css/**", "/img/**", "/fonts/**", "/js/**", "/", "/home", "/prizes", "/login",
            "/bet/get-selection", "/bet/place-bet", "/sport/**", "/competition/**", "/event/**",
            "/register", "/register/do")
        .permitAll().anyRequest().authenticated().and().formLogin().loginPage("/login")
        .loginProcessingUrl("/dologin").usernameParameter("username").passwordParameter("password")
        .defaultSuccessUrl("/loginsuccessful").failureUrl("/login?invalid").permitAll().and()
        .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/")
        .permitAll();
  }

}
