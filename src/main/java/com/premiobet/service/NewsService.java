package com.premiobet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.entity.News;
import com.premiobet.repository.NewsRepository;

@Service("newsService")
public class NewsService {
	
	@Autowired
	private NewsRepository newsRepository;
	
	public List<News> getLatestNews(){
		return newsRepository.findLatestNews();
	}

}
