package com.premiobet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.repository.TransactionRepository;

@Service("transactionService")
public class TransactionService {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	public int getRedeemedPrizesCount(){
		return transactionRepository.findRedeemedPrizesCount();
	}

}
