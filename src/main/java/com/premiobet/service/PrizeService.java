package com.premiobet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.repository.PrizeRepository;

@Service("prizeService")
public class PrizeService {
	
	@Autowired
	private PrizeRepository prizeRepository;
	
	public int getAvailablePrizesCount(){
		return prizeRepository.findAvailablePrizesCount();
	}

}
