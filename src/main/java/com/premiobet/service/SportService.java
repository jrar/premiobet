package com.premiobet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.entity.Sport;
import com.premiobet.model.SportModel;
import com.premiobet.repository.SportRepository;

@Service("sportService")
public class SportService {
	
	@Autowired
	private SportRepository sportRepository;
	
	public List<Sport> getSportsList(){
		return sportRepository.findAll();
	}
	
	public Sport getSingleSport(int sportId){
		return sportRepository.findById(sportId);
	}
	
	public Sport addSport(SportModel sportModel) {
		Sport sport = new Sport(sportModel.getSportName(), sportModel.getIconFile());
		sportRepository.save(sport);
		return sport;
	}
	
	public Sport updateSport(SportModel sportModel) {
		Sport sport = new Sport(sportModel.getId(), sportModel.getSportName(), sportModel.getIconFile());
		sportRepository.save(sport);
		return sport;
	}
	
	public void deleteSport(Sport sport) {
		sportRepository.delete(sport);
	}

}
