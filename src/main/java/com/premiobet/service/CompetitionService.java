package com.premiobet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.entity.Competition;
import com.premiobet.model.CompetitionModel;
import com.premiobet.repository.CompetitionRepository;

@Service("competitionService")
public class CompetitionService {
	
	@Autowired
	private CompetitionRepository competitionRepository;
	
	public List<Competition> getParentCompetitionsBySportId(int sportId) {
		return competitionRepository.findParentCompetitionsBySportId(sportId);
	}
	
	public List<Competition> getChildrenCompetitions(int parentId){
		return competitionRepository.findChildrenCompetitions(parentId);
	}
	
	public int getEventCountForSingleCompetition(int competitionId){
		return competitionRepository.getEventCountForSingleCompetition(competitionId);
	}
	
	public Competition getSingleCompetition(int id){
		return competitionRepository.findById(id);
	}
	
	public List<Competition> getAll() {
		return competitionRepository.findAll();
	}
	
	public Competition addCompetition(CompetitionModel competitionModel) {
		Competition competition = new Competition(competitionModel.getName(), competitionModel.getParentId(), competitionModel.getSportId());
		competitionRepository.save(competition);
		return competition;
	}
	
	public Competition updateCompetition(CompetitionModel competitionModel) {
		Competition competition = new Competition(competitionModel.getId(), competitionModel.getName(), competitionModel.getParentId(), competitionModel.getSportId());
		competitionRepository.save(competition);
		return competition;
	}
	
	public void deleteCompetition(Competition competition) {
		competitionRepository.delete(competition);
	}

}
