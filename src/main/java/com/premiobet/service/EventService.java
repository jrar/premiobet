package com.premiobet.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.premiobet.entity.Event;
import com.premiobet.model.EventModel;
import com.premiobet.repository.EventRepository;

@Service("eventService")
public class EventService {

  @Autowired
  private EventRepository eventRepository;

  public List<Event> getPopularEventsBySportId(int sportId, int limit) {
    return eventRepository.findPopularEventsBySportId(sportId, limit);
  }

  public List<Event> getPopularEventsFromAll(int limit) {
    return eventRepository.findPopularEventsFromAll(limit);
  }

  public List<Event> findUpcomingEventsBySportId(int sportId, int limit) {
    return eventRepository.findUpcomingEventsBySportId(sportId, limit);
  }

  public List<Event> getUpcomingEventsFromAll(int limit) {
    return eventRepository.findUpcomingEventsFromAll(limit);
  }

  public List<Event> getCompetitionEvents(int competitionId) {
    return eventRepository.findCompetitionEvents(competitionId);
  }

  public Event getSingleEvent(int eventId) {
    return eventRepository.findById(eventId);
  }

  public List<Event> getAllCompetitionEvents(int competitionId) {
    return eventRepository.findAllCompetitionEvents(competitionId);
  }

  public List<Event> getAllCompetitionEventsByRound(int competitionId, int round) {
    return eventRepository.findAllCompetitionEventsByRound(competitionId, round);
  }

  public List<Event> getPendingEvents() {
    return eventRepository.findPendingEvents();
  }

  public List<Event> getPendingEventsByCompetition(int competitionId) {
    return eventRepository.findPendingEventsByCompetition(competitionId);
  }

  public int countPendingEventsByCompetition(int competitionId) {
    return eventRepository.countPendingEventsByCompetition(competitionId);
  }

  public Event addEvent(EventModel eventModel) {
    Event event = new Event(eventModel.getCompetitionId(), eventModel.getHome(),
        eventModel.getAway(), eventModel.getDate(), eventModel.getRound());
    eventRepository.save(event);
    return event;
  }

  public Event updateEvent(EventModel eventModel) {
    Event event = new Event(eventModel.getId(), eventModel.getCompetitionId(), eventModel.getHome(),
        eventModel.getAway(), eventModel.getDate(), eventModel.getRound());
    eventRepository.save(event);
    return event;
  }

  public void deleteEvent(Event event) {
    eventRepository.delete(event);
  }

}
