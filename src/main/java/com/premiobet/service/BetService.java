package com.premiobet.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.premiobet.constant.AppSettings;
import com.premiobet.entity.Bet;
import com.premiobet.repository.BetRepository;

@Service("betService")
public class BetService {

  @Autowired
  private BetRepository betRepository;

  public List<Bet> getUserPendingBets(int userId) {
    return betRepository.findUserPendingBets(userId, AppSettings.PENDING_BETS_LIMIT);
  }

  public int getPendingBetsCount() {
    return betRepository.findPendingBetsCount();
  }

  public int getTotalBetsCount() {
    return betRepository.findTotalBetsCount();
  }

  public void updateWinnerBetBySelectionId(int selectionId) {
    betRepository.updateWinnerBetBySelectionId(selectionId);
  }

  public void updateLoserBetBySelectionId(int selectionId) {
    betRepository.updateLoserBetBySelectionId(selectionId);
  }

  public void updateCanceledBetBySelectionId(int selectionId) {
    betRepository.updateCanceledBetBySelectionId(selectionId);
  }

  public double getUserIncome(int selectionId, int userId) {
    return betRepository.findUserIncome(selectionId, userId);
  }

  public int[] getWinnerUsers(int selectionId) {
    return betRepository.findWinnerUsers(selectionId);
  }

}
