package com.premiobet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.premiobet.entity.Market;
import com.premiobet.repository.MarketRepository;

@Service("marketService")
public class MarketService {
	
	@Autowired
	private MarketRepository marketRepository;
	
	public int getMarketCountForSingleEvent(int eventId){
		return marketRepository.getMarketCountForSingleEvent(eventId);
	}
	
	public List<Market> getEventMarkets(int eventId){
		return marketRepository.findEventMarkets(eventId);
	}
	
	public Market getSingleMarket(int marketId){
		return marketRepository.findById(marketId);
	}
	
	public void updateMarketResult(int marketId, int result){
		marketRepository.updateMarketResult(marketId, result);
	}
	
	public void addMarket(Market market){
		marketRepository.save(market);
	}
	
	public Market getEventMainMarket(int eventId) {
		return marketRepository.findEventMainMarket(eventId);
	}

}
