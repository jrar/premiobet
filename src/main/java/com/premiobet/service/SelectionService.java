package com.premiobet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.premiobet.entity.Selection;
import com.premiobet.repository.SelectionRepository;

@Service("selectionService")
public class SelectionService {
	
	@Autowired
	private SelectionRepository selectionRepository;

	public List<Selection> getMainMarket(int eventId){
		return selectionRepository.findMainMarket(eventId);
	}
	
	public List<Selection> getMarketSelections(int marketId){
		return selectionRepository.findMarketSelections(marketId);
	}
	
	public List<Selection> getPendingMarketSelections(int marketId){
		return selectionRepository.findPendingMarketSelections(marketId);
	}
	
	public Selection getSingleSelection(int selectionId){
		return selectionRepository.findById(selectionId);
	}
	
	public void addSelection(Selection selection){
		selectionRepository.save(selection);
	}
	
	public Selection getMarketWinnerSelection(int marketId, int result) {
		return selectionRepository.getMarketWinnerSelection(marketId, result);
	}

}
