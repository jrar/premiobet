package com.premiobet.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.premiobet.constant.AppSettings;
import com.premiobet.entity.Bet;
import com.premiobet.entity.Event;
import com.premiobet.entity.Market;
import com.premiobet.entity.Selection;
import com.premiobet.entity.UserRole;
import com.premiobet.model.CouponBet;
import com.premiobet.model.RegistrationModel;
import com.premiobet.repository.BetRepository;
import com.premiobet.repository.UserRepository;

@Service("userService")
public class UserService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private BetRepository betRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    com.premiobet.entity.User user = userRepository.findByUsername(username);
    List<GrantedAuthority> authorities = buildAuthorities(user.getUserRole());
    return buildUser(user, authorities);
  }

  private User buildUser(com.premiobet.entity.User user, List<GrantedAuthority> authorities) {
    return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true,
        authorities);
  }

  private List<GrantedAuthority> buildAuthorities(Set<UserRole> userRoles) {
    Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
    for (UserRole ur : userRoles) {
      auths.add(new SimpleGrantedAuthority(ur.getRole()));
    }
    return new ArrayList<GrantedAuthority>(auths);
  }

  public com.premiobet.entity.User getUserEntity() {
    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    com.premiobet.entity.User fullUser = userRepository.findByUsername(user.getUsername());
    return fullUser;
  }

  public Bet placeBet(CouponBet bet, Selection selection, Market market, Event event) {
    Bet currentBet = new Bet(getUserEntity().getId(), event.getId(), market.getId(),
        selection.getId(), bet.getAmount(), bet.getOdd(), new Date(), 0, 0);
    betRepository.save(currentBet);
    updateBalance(currentBet.getAmount());
    return currentBet;
  }

  public boolean updateBalance(double betAmount) {
    com.premiobet.entity.User user = getUserEntity();
    if (user.getBalance() >= betAmount) {
      userRepository.updateBalance(betAmount, user.getId());
      return true;
    } else {
      return false;
    }
  }

  public void updateSessionUserBalance(double amount, HttpSession session) {
    com.premiobet.entity.User currentUser =
        (com.premiobet.entity.User) session.getAttribute("user");
    double currentBalance = currentUser.getBalance();
    currentUser.setBalance(currentBalance - amount);
  }

  public int getEnabledUsersCount() {
    return userRepository.findEnabledUsersCount();
  }

  public int getEnabledVerifiedUsersCount() {
    return userRepository.findEnabledVerifiedUsersCount();
  }

  public void addBalance(double amount, int userId) {
    userRepository.addBalance(amount, userId);
  }

  public double getUserBalance(int userId) {
    return userRepository.findUserBalance(userId);
  }

  public com.premiobet.entity.User register(RegistrationModel registrationModel) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    com.premiobet.entity.User user = new com.premiobet.entity.User(registrationModel.getUsername(),
        encoder.encode(registrationModel.getPassword()), registrationModel.getEmail(),
        AppSettings.NEW_USER_INITIAL_BALANCE, registrationModel.getFirstName(),
        registrationModel.getLastName(), registrationModel.getCountry(),
        registrationModel.getBirthDate(), true, false, new HashSet<UserRole>());
    userRepository.save(user);
    return user;
  }

}
