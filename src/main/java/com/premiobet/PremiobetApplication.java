package com.premiobet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PremiobetApplication {

	public static void main(String[] args) {
		SpringApplication.run(PremiobetApplication.class, args);
	}
	
}
