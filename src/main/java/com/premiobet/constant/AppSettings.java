package com.premiobet.constant;

public class AppSettings {

  public static final double MIN_BET_AMOUNT = 10;
  public static final double MAX_BET_AMOUNT = 100000;
  public static final int PENDING_BETS_LIMIT = 10;
  public static final int POPULAR_EVENTS_LIMIT = 6;
  public static final int UPCOMING_EVENTS_LIMIT = 6;
  public static final double NEW_USER_INITIAL_BALANCE = 1000;

}
