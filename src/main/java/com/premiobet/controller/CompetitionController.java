package com.premiobet.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.entity.Event;
import com.premiobet.model.EventDetail;
import com.premiobet.service.CompetitionService;
import com.premiobet.service.EventService;
import com.premiobet.service.MarketService;
import com.premiobet.service.SelectionService;
import com.premiobet.service.SportService;

@Controller
@RequestMapping("/competition")
public class CompetitionController {

  @Autowired
  private SportService sportService;

  @Autowired
  private CompetitionService competitionService;

  @Autowired
  private EventService eventService;

  @Autowired
  private MarketService marketService;

  @Autowired
  private SelectionService selectionService;

  @GetMapping("/{competitionId}")
  public ModelAndView getSingleCompetitionPage(@PathVariable("competitionId") int competitionId) {

    ModelAndView mav = new ModelAndView("competition");

    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("currentCompetition", competitionService.getSingleCompetition(competitionId));

    List<Event> popularEvents = eventService.getCompetitionEvents(competitionId);
    List<EventDetail> popularEventViewObjectList = new ArrayList<>();

    for (Event event : popularEvents) {
      EventDetail popularEventViewObject =
          new EventDetail(event, selectionService.getMainMarket(event.getId()),
              marketService.getMarketCountForSingleEvent(event.getId()));
      popularEventViewObjectList.add(popularEventViewObject);
    }

    mav.addObject("popularEvents", popularEventViewObjectList);

    return mav;
  }

}
