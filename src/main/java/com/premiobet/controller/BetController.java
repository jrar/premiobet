package com.premiobet.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.constant.AppSettings;
import com.premiobet.entity.Bet;
import com.premiobet.entity.Event;
import com.premiobet.entity.Market;
import com.premiobet.entity.Selection;
import com.premiobet.entity.User;
import com.premiobet.model.CouponBet;
import com.premiobet.model.CouponSelection;
import com.premiobet.model.PendingBet;
import com.premiobet.service.BetService;
import com.premiobet.service.EventService;
import com.premiobet.service.MarketService;
import com.premiobet.service.SelectionService;
import com.premiobet.service.UserService;

@Controller
@RequestMapping("/bet")
public class BetController {

  @Autowired
  private EventService eventService;

  @Autowired
  private MarketService marketService;

  @Autowired
  private SelectionService selectionService;

  @Autowired
  private UserService userService;

  @Autowired
  private BetService betService;

  @PostMapping("/get-selection")
  public @ResponseBody ModelAndView getSelection(@RequestBody String selectionId) {
    ModelAndView mav = new ModelAndView("fragments/common :: div.new-selection");
    Selection selection = selectionService.getSingleSelection(Integer.parseInt(selectionId));
    Market market = marketService.getSingleMarket(selection.getMarket());
    Event event = eventService.getSingleEvent(market.getEvent());
    CouponSelection couponSelection = new CouponSelection(selection, market.getName(),
        event.getHome() + " vs " + event.getAway());
    mav.addObject("couponSelection", couponSelection);
    return mav;
  }

  @PostMapping("/place-bet")
  public @ResponseBody ModelAndView placeBet(@RequestBody CouponBet bet, HttpSession session,
      Principal principal) {
    ModelAndView mav = new ModelAndView();
    Selection selection = selectionService.getSingleSelection(bet.getSelectionId());
    Market market = marketService.getSingleMarket(selection.getMarket());
    Event event = eventService.getSingleEvent(market.getEvent());
    CouponSelection couponSelection = new CouponSelection(selection, market.getName(),
        event.getHome() + " vs " + event.getAway());
    mav.addObject("couponSelection", couponSelection);

    // check if user is logged-in
    if (principal != null) {
      // check if event date is lower than current date
      if (event.getStartDate().after(new Date())) {
        // check if bet amount is valid
        if (bet.getAmount() >= AppSettings.MIN_BET_AMOUNT
            && bet.getAmount() <= AppSettings.MAX_BET_AMOUNT) {
          // check if user balance is greather than bet amount
          if (userService.getUserEntity().getBalance() >= bet.getAmount()) {
            // check if selection odd did change after user added it
            // to coupon
            if (bet.getOdd() == selectionService.getSingleSelection(bet.getSelectionId())
                .getOdd()) {
              // everything OK, place bet and return bet success
              // fragment
              Bet currentBet = userService.placeBet(bet, selection, market, event);
              userService.updateSessionUserBalance(bet.getAmount(), session);
              mav.addObject("currentBet", currentBet);
              mav.setViewName("fragments/common :: div.place-bet-success");
            } else {
              // return odd changed coupon fragment
              mav.setViewName("fragments/common :: div.place-bet-odd-changed");
            }
          } else {
            // return not enought balance coupon fragment
            mav.setViewName("fragments/common :: div.place-bet-lower-balance");
          }
        } else {
          // return invalid amount coupon fragment
          mav.addObject("minBetAmount", Math.round(AppSettings.MIN_BET_AMOUNT));
          mav.addObject("maxBetAmount", Math.round(AppSettings.MAX_BET_AMOUNT));
          mav.setViewName("fragments/common :: div.place-bet-invalid-amount");
        }
      } else {
        // return not logged in coupon fragment
        mav.setViewName("fragments/common :: div.place-bet-unavailable-selection");
      }
    } else {
      // return unavailable selection coupon fragment
      mav.setViewName("fragments/common :: div.place-bet-not-logged");
    }

    return mav;
  }

  @PostMapping("/refresh-pending-bets")
  public @ResponseBody ModelAndView refreshPendingBets(Principal principal) {
    User currentUser = null;
    if (principal != null && !principal.equals("anonymousUser")) {
      currentUser = userService.getUserEntity();
    }
    ModelAndView mav = new ModelAndView("fragments/common :: div#pending-bets");
    List<Bet> userPendingBets = betService.getUserPendingBets(currentUser.getId());
    List<PendingBet> userPendingBetsViewObjectList = new ArrayList<>();

    for (Bet bet : userPendingBets) {
      PendingBet pendingBet =
          new PendingBet(bet, selectionService.getSingleSelection(bet.getSelectionId()).getName(),
              eventService.getSingleEvent(bet.getEventId()).getHome() + " vs "
                  + eventService.getSingleEvent(bet.getEventId()).getAway(),
              marketService.getSingleMarket(bet.getMarketId()).getName());
      userPendingBetsViewObjectList.add(pendingBet);
    }
    mav.addObject("pendingBets", userPendingBetsViewObjectList);
    return mav;
  }
}
