package com.premiobet.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.entity.Competition;
import com.premiobet.entity.Event;
import com.premiobet.entity.Market;
import com.premiobet.entity.Selection;
import com.premiobet.entity.Sport;
import com.premiobet.model.AdminStats;
import com.premiobet.model.CloseEventModel;
import com.premiobet.model.CloseMarketModel;
import com.premiobet.model.CompetitionModel;
import com.premiobet.model.EventModel;
import com.premiobet.model.MarketModel;
import com.premiobet.model.SportModel;
import com.premiobet.service.BetService;
import com.premiobet.service.CompetitionService;
import com.premiobet.service.EventService;
import com.premiobet.service.MarketService;
import com.premiobet.service.PrizeService;
import com.premiobet.service.SelectionService;
import com.premiobet.service.SportService;
import com.premiobet.service.TransactionService;
import com.premiobet.service.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController {

  @Autowired
  private SportService sportService;

  @Autowired
  private CompetitionService competitionService;

  @Autowired
  private EventService eventService;

  @Autowired
  private UserService userService;

  @Autowired
  private BetService betService;

  @Autowired
  private PrizeService prizeService;

  @Autowired
  private TransactionService transactionService;

  @Autowired
  private MarketService marketService;

  @Autowired
  private SelectionService selectionService;

  @GetMapping({"/", ""})
  public ModelAndView getAdminPanel() {
    ModelAndView mav = new ModelAndView("admin/admin-panel");
    AdminStats stats = new AdminStats(userService.getEnabledUsersCount(),
        userService.getEnabledVerifiedUsersCount(), betService.getPendingBetsCount(),
        betService.getTotalBetsCount(), prizeService.getAvailablePrizesCount(),
        transactionService.getRedeemedPrizesCount());
    mav.addObject("stats", stats);
    return mav;
  }

  @GetMapping("/manage-sports")
  public ModelAndView getManageSports() {
    ModelAndView mav = new ModelAndView("admin/manage-sports");
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("sportModel", new SportModel());
    return mav;
  }

  @PostMapping("/sports/add")
  public ModelAndView addSport(@ModelAttribute("sportModel") SportModel sportModel) {
    ModelAndView mav = new ModelAndView("admin/manage-sports");

    Sport newSport = sportService.addSport(sportModel);
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("sportModel", new SportModel());

    if (newSport != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @PostMapping("/sports/edit")
  public ModelAndView getEditSport(@RequestParam(value = "sport") int sportId) {
    ModelAndView mav = new ModelAndView("admin/edit-sport");

    Sport sport = sportService.getSingleSport(sportId);
    SportModel currentSport = new SportModel(sport.getName(), sport.getImg(), sportId);
    mav.addObject("currentSport", currentSport);
    mav.addObject("sportModel", new SportModel());

    return mav;
  }

  @PostMapping("/sports/edit/do")
  public ModelAndView editSport(@ModelAttribute("sportModel") SportModel sportModel) {
    ModelAndView mav = new ModelAndView("admin/manage-sports");
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("sportModel", new SportModel());

    Sport sport = sportService.updateSport(sportModel);

    if (sport != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @PostMapping("/sports/delete")
  public ModelAndView deleteSport(@RequestParam(value = "deleteSport") int sportId) {
    ModelAndView mav = new ModelAndView("admin/manage-sports");

    sportService.deleteSport(sportService.getSingleSport(sportId));
    mav.addObject("deleted", true);
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("sportModel", new SportModel());

    return mav;
  }

  @GetMapping("/manage-competitions")
  public ModelAndView getManageCompetitions() {
    ModelAndView mav = new ModelAndView("admin/manage-competitions");
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("competitionModel", new CompetitionModel());
    return mav;
  }

  @PostMapping("/competitions/add")
  public ModelAndView addCompetition(
      @ModelAttribute("competitionModel") CompetitionModel competitionModel) {
    ModelAndView mav = new ModelAndView("admin/manage-competitions");

    Competition newCompetition = competitionService.addCompetition(competitionModel);
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("competitionModel", new CompetitionModel());

    if (newCompetition != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @PostMapping("/competitions/edit")
  public ModelAndView getEditCompetition(@RequestParam(value = "competition") int competitionId) {
    ModelAndView mav = new ModelAndView("admin/edit-competition");

    Competition competition = competitionService.getSingleCompetition(competitionId);
    CompetitionModel currentCompetition = new CompetitionModel(competitionId, competition.getName(),
        competition.getParentId(), competition.getSport());
    mav.addObject("currentCompetition", currentCompetition);
    mav.addObject("competitionModel", new CompetitionModel());
    mav.addObject("currentCompetitionSportName",
        sportService.getSingleSport(competition.getSport()).getName());
    if (competition.getParentId() != 0) {
      mav.addObject("currentCompetitionParentName",
          competitionService.getSingleCompetition(competition.getParentId()).getName());
    } else {
      mav.addObject("currentCompetitionParentName", "None");
    }
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("sports", sportService.getSportsList());

    return mav;
  }

  @PostMapping("/competitions/edit/do")
  public ModelAndView editCompetition(
      @ModelAttribute("sportModel") CompetitionModel competitionModel) {
    ModelAndView mav = new ModelAndView("admin/manage-competitions");
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("competitionModel", new CompetitionModel());

    Competition competition = competitionService.updateCompetition(competitionModel);

    if (competition != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @PostMapping("/competitions/delete")
  public ModelAndView deleteCompetition(
      @RequestParam(value = "deleteCompetition") int competitionId) {
    ModelAndView mav = new ModelAndView("admin/manage-competitions");

    competitionService.deleteCompetition(competitionService.getSingleCompetition(competitionId));
    mav.addObject("deleted", true);
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("competitionModel", new CompetitionModel());

    return mav;
  }

  @GetMapping("/manage-events")
  public ModelAndView getManageEvents() {
    ModelAndView mav = new ModelAndView("admin/manage-events");
    mav.addObject("competitions", competitionService.getChildrenCompetitions(0));
    mav.addObject("eventModel", new EventModel());
    return mav;
  }

  @PostMapping("/get-competition-events")
  public @ResponseBody ModelAndView getCompetitionEvents(@RequestBody String competitionId) {
    ModelAndView mav = new ModelAndView("fragments/admin :: select#event");
    mav.addObject("events", eventService.getAllCompetitionEvents(Integer.parseInt(competitionId)));
    return mav;
  }

  @PostMapping("/events/add")
  public ModelAndView addEvent(@ModelAttribute("eventModel") EventModel eventModel) {
    ModelAndView mav = new ModelAndView("admin/manage-events");

    Event newEvent = eventService.addEvent(eventModel);
    mav.addObject("competitions", competitionService.getChildrenCompetitions(0));
    mav.addObject("eventModel", new EventModel());

    if (newEvent != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @PostMapping("/events/edit")
  public ModelAndView getEditEvent(@RequestParam(value = "event") int eventId) {
    ModelAndView mav = new ModelAndView("admin/edit-event");

    Event event = eventService.getSingleEvent(eventId);
    EventModel currentEvent = new EventModel(eventId, event.getCompetitionId(), event.getHome(),
        event.getAway(), event.getStartDate(), event.getRound());
    mav.addObject("currentEvent", currentEvent);
    mav.addObject("eventModel", new EventModel());
    mav.addObject("currentEventCompetitionName",
        competitionService.getSingleCompetition(event.getCompetitionId()).getName());
    mav.addObject("competitions", competitionService.getAll());

    return mav;
  }

  @PostMapping("/events/edit/do")
  public ModelAndView editEvent(@ModelAttribute("eventModel") EventModel eventModel) {
    ModelAndView mav = new ModelAndView("admin/manage-events");
    mav.addObject("competitions", competitionService.getAll());
    mav.addObject("eventModel", new EventModel());

    Event event = eventService.updateEvent(eventModel);

    if (event != null) {
      mav.addObject("success", true);
    } else {
      mav.addObject("success", false);
    }
    return mav;
  }

  @GetMapping("/close-event")
  public ModelAndView getCloseEvent() {
    ModelAndView mav = new ModelAndView("admin/close-event");
    mav.addObject("competitions", competitionService.getAll());
    return mav;
  }

  @PostMapping("/events/close")
  public ModelAndView closeEvent(@RequestParam(value = "event") int eventId) {
    ModelAndView mav = new ModelAndView("admin/close-event-markets");
    mav.addObject("currentEvent", eventService.getSingleEvent(eventId));

    List<Market> eventMarkets = marketService.getEventMarkets(eventId);
    List<CloseEventModel> eventMarketModels = new ArrayList<>();

    for (Market market : eventMarkets) {
      eventMarketModels
          .add(new CloseEventModel(market, selectionService.getMarketSelections(market.getId())));
    }
    mav.addObject("eventMarkets", eventMarketModels);

    return mav;
  }

  @PostMapping("/events/close/do")
  public @ResponseBody ModelAndView closeMarket(@RequestBody CloseMarketModel marketModel) {
    ModelAndView mav = new ModelAndView("fragments/admin :: #market-closed-successfully");
    marketService.updateMarketResult(marketModel.getMarketId(), marketModel.getSelectionId());
    betService.updateWinnerBetBySelectionId(marketModel.getSelectionId());

    int[] winnerUsers = betService.getWinnerUsers(marketModel.getSelectionId());

    for (int userId : winnerUsers) {
      double income = betService.getUserIncome(marketModel.getSelectionId(), userId);
      userService.addBalance(income, userId);
    }

    return mav;
  }

  @GetMapping("/manage-markets")
  public ModelAndView getManageSelections() {
    ModelAndView mav = new ModelAndView("admin/manage-markets");
    mav.addObject("competitions", competitionService.getAll());
    return mav;
  }

  @PostMapping("/events/addmarket")
  public ModelAndView getAddMarket(@RequestParam(value = "event") int eventId) {
    ModelAndView mav = new ModelAndView("admin/add-market");

    Event event = eventService.getSingleEvent(eventId);
    EventModel currentEvent = new EventModel(eventId, event.getCompetitionId(), event.getHome(),
        event.getAway(), event.getStartDate(), event.getRound());
    mav.addObject("currentEvent", currentEvent);
    mav.addObject("marketModel", new MarketModel());

    return mav;
  }

  @PostMapping("/events/addmarket/do")
  public ModelAndView addMarket(@ModelAttribute("marketModel") MarketModel marketModel) {
    ModelAndView mav = new ModelAndView("admin/manage-markets");

    Market market =
        new Market(marketModel.getName(), marketModel.getCategory(), marketModel.getEventId());
    marketService.addMarket(market);

    if (marketModel.getDraw() != null && !marketModel.getDraw().equals("")) {
      Selection home =
          new Selection(marketModel.getHome(), market.getId(), marketModel.getHomeOdds(), 1);
      Selection away =
          new Selection(marketModel.getAway(), market.getId(), marketModel.getAwayOdds(), 3);
      Selection draw =
          new Selection(marketModel.getDraw(), market.getId(), marketModel.getDrawOdds(), 2);
      selectionService.addSelection(draw);
      selectionService.addSelection(home);
      selectionService.addSelection(away);
    } else {
      Selection home =
          new Selection(marketModel.getHome(), market.getId(), marketModel.getHomeOdds(), 1);
      Selection away =
          new Selection(marketModel.getAway(), market.getId(), marketModel.getAwayOdds(), 2);
      selectionService.addSelection(home);
      selectionService.addSelection(away);
    }

    mav.addObject("competitions", competitionService.getAll());

    return mav;
  }

}
