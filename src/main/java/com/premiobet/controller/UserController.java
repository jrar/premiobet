package com.premiobet.controller;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import com.premiobet.entity.User;
import com.premiobet.model.RegistrationModel;
import com.premiobet.service.UserService;

@Controller
public class UserController {

  @Autowired
  private UserService userService;

  @GetMapping("/login")
  public ModelAndView getLogin() {
    ModelAndView mav = new ModelAndView("login");
    return mav;
  }

  @GetMapping("/loginsuccessful")
  public RedirectView loadUserOnSession(HttpSession session) {
    session.setAttribute("user", userService.getUserEntity());
    return new RedirectView("/");
  }

  @GetMapping("/register")
  public ModelAndView getRegister() {
    ModelAndView mav = new ModelAndView("register");
    mav.addObject("registrationModel", new RegistrationModel());
    return mav;
  }

  @PostMapping("/register/do")
  public ModelAndView doRegister(
      @ModelAttribute("registrationModel") RegistrationModel registrationModel) {
    userService.register(registrationModel);
    ModelAndView mav = new ModelAndView("login");
    mav.addObject("registered", true);
    return mav;
  }

  @PostMapping("/user/refresh-balance")
  public @ResponseBody ModelAndView refreshBalance(HttpSession session) {
    ModelAndView mav = new ModelAndView();
    double userBalance = userService.getUserBalance(userService.getUserEntity().getId());
    User user = (User) session.getAttribute("user");
    user.setBalance(userBalance);
    session.setAttribute("user", user);
    mav.setViewName("fragments/common :: span#user-balance");
    return mav;
  }

  @GetMapping("/account")
  public ModelAndView getUserAccount() {
    ModelAndView mav = new ModelAndView("account");
    return mav;
  }

}
