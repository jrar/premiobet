package com.premiobet.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.entity.Competition;
import com.premiobet.model.SportCompetition;
import com.premiobet.service.CompetitionService;
import com.premiobet.service.SportService;

@Controller
@RequestMapping("/sport")
public class SportController {

  @Autowired
  private SportService sportService;

  @Autowired
  private CompetitionService competitionService;

  @GetMapping("/{sportId}")
  public ModelAndView getSportEventsPage(@PathVariable("sportId") int sportId) {

    ModelAndView mav = new ModelAndView("sport");

    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("currentSport", sportService.getSingleSport(sportId));

    List<Competition> sportCompetitions =
        competitionService.getParentCompetitionsBySportId(sportId);
    List<SportCompetition> sportCompetitionViewObjectList = new ArrayList<>();

    for (Competition competition : sportCompetitions) {
      SportCompetition sportCompetitionViewObject = new SportCompetition(competition,
          competitionService.getChildrenCompetitions(competition.getId()),
          competitionService.getEventCountForSingleCompetition(competition.getId()));
      sportCompetitionViewObjectList.add(sportCompetitionViewObject);
    }

    mav.addObject("sportCompetitions", sportCompetitionViewObjectList);

    return mav;
  }

}
