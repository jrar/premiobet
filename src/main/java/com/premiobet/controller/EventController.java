package com.premiobet.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.entity.Market;
import com.premiobet.model.EventMarket;
import com.premiobet.service.EventService;
import com.premiobet.service.MarketService;
import com.premiobet.service.SelectionService;
import com.premiobet.service.SportService;

@Controller
@RequestMapping("/event")
public class EventController {

  @Autowired
  private SportService sportService;

  @Autowired
  private EventService eventService;

  @Autowired
  private MarketService marketService;

  @Autowired
  private SelectionService selectionService;

  @GetMapping("/{eventId}")
  public ModelAndView getSingleEventPage(@PathVariable("eventId") int eventId) {

    ModelAndView mav = new ModelAndView("event");

    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("currentEvent", eventService.getSingleEvent(eventId));

    List<Market> eventMarkets = marketService.getEventMarkets(eventId);
    List<EventMarket> eventMarketViewObjectList = new ArrayList<>();

    for (Market market : eventMarkets) {
      EventMarket eventMarketViewObject =
          new EventMarket(market, selectionService.getMarketSelections(market.getId()));
      eventMarketViewObjectList.add(eventMarketViewObject);
    }

    mav.addObject("eventMarkets", eventMarketViewObjectList);

    return mav;
  }

}
