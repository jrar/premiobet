package com.premiobet.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.premiobet.constant.AppSettings;
import com.premiobet.entity.Bet;
import com.premiobet.entity.Event;
import com.premiobet.entity.User;
import com.premiobet.model.PendingBet;
import com.premiobet.model.EventDetail;
import com.premiobet.service.BetService;
import com.premiobet.service.EventService;
import com.premiobet.service.MarketService;
import com.premiobet.service.NewsService;
import com.premiobet.service.SelectionService;
import com.premiobet.service.SportService;
import com.premiobet.service.UserService;

@Controller
public class IndexController {

  @Autowired
  private UserService userService;

  @Autowired
  private SportService sportService;

  @Autowired
  private NewsService newsService;

  @Autowired
  private EventService eventService;

  @Autowired
  private MarketService marketService;

  @Autowired
  private SelectionService selectionService;

  @Autowired
  private BetService betService;

  @GetMapping({"/", "/home"})
  public ModelAndView getIndex(Principal principal) {

    // Retrieve user data if logged in
    User currentUser = null;
    if (principal != null && !principal.equals("anonymousUser")) {
      currentUser = userService.getUserEntity();
    }

    // Set view and retrieve sidebar sports list and carousel news
    ModelAndView mav = new ModelAndView("index");
    mav.addObject("sports", sportService.getSportsList());
    mav.addObject("news", newsService.getLatestNews());

    // Retrieve popular events and map them to PopularEvent model. Add these DTOs to MAV
    List<Event> popularEvents =
        eventService.getPopularEventsFromAll(AppSettings.POPULAR_EVENTS_LIMIT);
    List<EventDetail> popularEventViewObjectList = new ArrayList<>();

    for (Event event : popularEvents) {
      EventDetail popularEventViewObject =
          new EventDetail(event, selectionService.getMainMarket(event.getId()),
              marketService.getMarketCountForSingleEvent(event.getId()));
      popularEventViewObjectList.add(popularEventViewObject);
    }

    mav.addObject("popularEvents", popularEventViewObjectList);

    // Retrieve upcoming events and map them to PopularEvent model. Add these DTOs to MAV
    List<Event> upcomingEvents =
        eventService.getUpcomingEventsFromAll(AppSettings.UPCOMING_EVENTS_LIMIT);
    List<EventDetail> upcomingEventViewObjectList = new ArrayList<>();

    for (Event event : upcomingEvents) {
      EventDetail upcomingEventViewObject =
          new EventDetail(event, selectionService.getMainMarket(event.getId()),
              marketService.getMarketCountForSingleEvent(event.getId()));
      upcomingEventViewObjectList.add(upcomingEventViewObject);
    }

    mav.addObject("upcomingEvents", upcomingEventViewObjectList);

    // Retrieve user pending bets and add them to MAV (sidebar widget)
    if (currentUser != null) {
      List<Bet> userPendingBets = betService.getUserPendingBets(currentUser.getId());
      List<PendingBet> userPendingBetsViewObjectList = new ArrayList<>();

      for (Bet bet : userPendingBets) {
        PendingBet pendingBet =
            new PendingBet(bet, selectionService.getSingleSelection(bet.getSelectionId()).getName(),
                eventService.getSingleEvent(bet.getEventId()).getHome() + " vs "
                    + eventService.getSingleEvent(bet.getEventId()).getAway(),
                marketService.getSingleMarket(bet.getMarketId()).getName());
        userPendingBetsViewObjectList.add(pendingBet);
      }

      mav.addObject("pendingBets", userPendingBetsViewObjectList);
    }

    return mav;
  }

  @PostMapping("/refresh-date")
  public @ResponseBody ModelAndView refreshDate() {
    ModelAndView mav = new ModelAndView("fragments/common :: span#server-date");
    return mav;
  }

}
