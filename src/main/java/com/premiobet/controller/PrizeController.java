package com.premiobet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/prizes")
public class PrizeController {

  @GetMapping("/")
  public ModelAndView getPrizes() {
    ModelAndView mav = new ModelAndView("prizes");
    return mav;
  }

}
